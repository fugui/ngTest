import { Component } from '@angular/core';

@Component({
  selector: 'app-lost',
  template: `<div>404</div>`,
})
export class NotFoundComponent {
  title = '404';
}
