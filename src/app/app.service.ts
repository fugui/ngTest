import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export default class AppService {
  count = 0;
  count$ = new BehaviorSubject<number>(this.count);

  inc() {
    this.count++;
    this.updateCountObj(this.count);
  }

  dec() {
    this.count--;
    this.updateCountObj(this.count);
  }

  get getCount(): number {
    return this.count;
  }

  private updateCountObj(value) {
    this.count$.next(value);
  }
}
