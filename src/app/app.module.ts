import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NotFoundComponent } from './app.component.404';
import { TodoListComponent } from '../todo/todo.component';
import { TodoDetailComponent } from '../todo/todoDetail.component';
import { TodoEditComponent } from '../todo/todoEdit.component';
import AppService from './app.service';

const appRoutes: Routes = [
  {
    path: 'todo/:id',
    component: TodoDetailComponent
  },
  {
    path: 'todo',
    component: TodoListComponent
  },
  {
    path: 'edit/:id?',
    component: TodoEditComponent
  },
  {
    path: 'create',
    component: TodoEditComponent
  },
  {
    path: '',
    redirectTo: '/todo',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
@NgModule({
  declarations: [AppComponent, NotFoundComponent, TodoListComponent, TodoDetailComponent, TodoEditComponent],
  imports: [FormsModule, BrowserModule, RouterModule.forRoot(appRoutes, { enableTracing: false })],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule {}
