import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import AppService from '../app/app.service';

@Component({
  selector: 'app-todo',
  templateUrl: './count.templete.html'
})
export class TodoEditComponent implements OnInit, OnDestroy {
  title = 'edit';
  countSub: Subscription;
  constructor(private counter: AppService) {}
  ngOnInit() {
    this.countSub = this.counter.count$.subscribe(value => {
      console.log('全局监听counter', value);
    });
  }
  ngOnDestroy() {
    this.countSub.unsubscribe();
  }
}
