import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
// import { Observable } from 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html'
})
export class TodoListComponent implements OnInit, OnChanges {
  constructor() {}
  title = 'TodoListComponent';
  data = [];
  edit = '';
  public items = Observable.of([1, 2, 3]);
  ngOnInit() {
    console.log('init');
  }
  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        const chng = changes[propName];
        const cur = JSON.stringify(chng.currentValue);
        const prev = JSON.stringify(chng.previousValue);
        console.log(cur, prev);
      }
    }
  }
  toggle(index) {
    this.data[index].isComplete = !this.data[index].isComplete;
  }
  get hasTodo() {
    return this.data.length > 0;
  }
  get finished() {
    console.log(this.data.filter(d => d.isComplete).length);
    return this.data.filter(d => d.isComplete).length;
  }
  add() {
    const newTodo = {
      name: this.edit,
      isComplete: false,
      logs: []
    };
    this.data.unshift(newTodo);
    this.edit = '';
  }
  remove(index) {
    this.data.splice(index, 1);
  }
}
